const { Schema, model } = require('mongoose');
const mongoose = require('mongoose');

const OfferResponseSchema = Schema({
  taxi: {
    type: mongoose.Schema.Types.ObjectId,
    required: [true, "The taxi is required"]
  },
  priceOffer: {
    type: Number,
    require: [true, "The priceOffer is required"]
  },
});

module.exports = mongoose.model('OfferResponse', OfferResponseSchema);