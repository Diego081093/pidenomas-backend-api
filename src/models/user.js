const { Schema } = require('mongoose');
const mongoose = require('mongoose');

const UserSchema = Schema({
    name: {
        type: String,
        required: [true, 'The name is required.']
    },
    email: {
        type: String,
        required: [true, 'The email is required.'],
        unique: true
    },
    password: {
        type: String,
        required: [true, 'The password is required.'],
    },
    img: {
        type: String,
        default: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png',
    },
    dni: {
        type: String,
        default: '00000000',
        unique: true,
    },
    phone: {
        type: String,
        required: [true, 'The phone is required.']
    },
    gender: {
        type: String,
        required: [true, 'The gender in required.']
    },
    birthday: {
        type: String,
        default: '00/00/0000',
    },
    rol: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Role',
        required: [true, 'Assign a role'],
    },
    state: {
        type: Boolean,
        default: true
    },
    google: {
        type: Boolean,
        default: false
    },
}, {
    timestamps: true,
});

UserSchema.methods.toJSON = function() {
    const { __v, password, ...user  } = this.toObject();
    return user;
}

module.exports = mongoose.model( 'User', UserSchema );
