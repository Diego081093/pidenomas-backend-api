const { Schema, model } = require('mongoose');
const mongoose = require('mongoose');

const CategorySchema = Schema({
    idCat: {
        type: String,
    },
    name: {
        type: String,
        required: [true, 'The category name is required.'],
    },
    image: {
        type: String,
        require: [true, 'The image is required.'],
    },
    state: {
        type: Boolean,
        default: true,
    },
}, {
    timestamps: true,
});


module.exports = mongoose.model( 'Category', CategorySchema );
