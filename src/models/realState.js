const { Schema, model } = require('mongoose');
const mongoose = require('mongoose');

const RealStateSchema = Schema({
    name: {
        type: String,
        required: [true, 'The name is required.'],
    },
    description: {
        type: String,
        required: [true, 'The description is required.'],
    },
    image: {
        type: String,
        //required: [true, 'The image is required.'],
    },
    address: {
        type: String,
        required: [true, "The address is required."],
    },
    city: {
        type: String,
        required: [true, "The city is required"],
    },
    district: {
        type: String,
        required: [true, "The district is required"],
    },
    price: {
        type: Number,
        required: [true, 'The price is required.'],
    },
    area: {
        type: Number,
        required: [true, 'The area is required.'],
    },
    bedrooms: {
        type: Number,
        required: [true, 'The bedrooms is required.'],
    },
    toilets: {
        type: Number,
        required: [true, 'The toilets is required.'],
    },
    type: {
        type: Number,
        required: [true, 'The type is required.'],
    },
    state: {
        type: Boolean,
        default: true,
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: [true, "This store must be assigned to a user"],
      },
}, {
    timestamps: true,
});

module.exports = mongoose.model( 'RealState', RealStateSchema );
