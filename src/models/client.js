const { Schema, model } = require('mongoose');
const mongoose = require('mongoose');

const ClientSchema = Schema({
    idCat: {
        type: String
    },
    name: {
        type: String
    },
    plan: {
        type: String
    },
    local: {
        type: String
    },
    address: {
        type: String
    },
    state: {
        type: Boolean,
        default: true,
    },
}, {
    timestamps: true,
});


module.exports = mongoose.model( 'Client', ClientSchema );
