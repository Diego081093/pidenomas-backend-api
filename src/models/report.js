const { Schema, model } = require('mongoose');
const mongoose = require('mongoose');

const ReportSchema = Schema({
    description: {
        type: String,
        required: [true, 'The description ir required.']
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    }
}, {
  timestamps: true,
});

module.exports = mongoose.model( 'Report', ReportSchema );
