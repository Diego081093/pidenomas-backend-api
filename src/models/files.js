const { Schema } = require('mongoose');
const mongoose = require('mongoose');

const FileSchema = Schema({
  sectionName: {
    type: String,
    unique : true
  },
  idStore: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Store'
  },
  urlFile: {
    type: String
  }
});

module.exports = mongoose.model('Files', FileSchema);