const { Schema, model } = require('mongoose');
const mongoose = require('mongoose');

const TaxiSchema = Schema({
  currentLocation: {
    longitude: {
      type: String,
      require: [true, "The current longitude is required"],
      default: "0.0"
    },
    latitude: {
      type: String,
      require: [true, "The current latitude is required"],
      default: "0.0"
    },
    address: {
      type: String,
      require: [true, "The current address is required"],
      default: ""
    }
  },
  isBidding: {
    type: Boolean,
    require: [true, "The bidding is required"],
    default: false
  },
  isActive: {
    type: Boolean,
    default: false,
  },
  carDetails: {
    brand: {
      type: String,
      require: [true, "The brand is required"]
    },
    model: {
      type: String,
      require: [true, "The model is required"]
    },
    color: {
      type: String,
      require: [true, "The color is required"]
    },
    tuition: {
      type: String,
      require: [true, "The tuition is required"]
    },
    imageCar: {
      type: String,
      require: [true, "The imageCar is required"]
    },
    imageCardProperty: {
      type: String,
      require: [true, "The imageCar is required"]
    },
    imageIdentification: {
      front: {
        type: String,
        require: [true, "The imageCar is required"]
      },
      reverse: {
        type: String,
        require: [true, "The imageCar is required"]
      }
    },
    imageCardLicense: {
      type: String,
      require: [true, "The imageCar is required"]
    },
    imageCardSoat: {
      type: String,
      require: [true, "The imageCar is required"]
    }
  },
  driver: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    require: [true, "The driver is required"]
  }
}, {
  timestamps: true,
});

module.exports = mongoose.model('Taxi', TaxiSchema);