const { Schema } = require('mongoose');
const mongoose = require('mongoose');
const { customAlphabet } = require('nanoid');

const nanoid = customAlphabet('1234567890', 10);

const OrderSchema = Schema({
  orderNumber: {
    type: String,
    default: nanoid(10),
  },
  products: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: 'Product',
  },
  totalPrice: {
    type: Number,
    required: [true, 'The total is required'],
  },
  addressDelivery: {
    type: String,
    required: [true, 'The coordinates delivery is required'],
  },
  client: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  store: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Store',
  },
  isCashPayment: {
    type: Boolean,
    required: [true, 'The cashPayment is required'],
  },
  numberPayment: {
    type: String,
    required: [false, ''],
    default: '',
  },
  screenShotPay: {
    type: String,
    required: [false, ''],
    default: '',
  },
  state: {
    type: Number,
    default: 1,
  }
}, {
  timestamps: true,
});

module.exports = mongoose.model('Order', OrderSchema);