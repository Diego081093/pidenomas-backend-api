const { Schema } = require('mongoose');
const mongoose = require('mongoose');

const SectionSchema = Schema({
  name: {
    type: String,
    required: [true, "The description is required."],
  },
  products: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: 'Product',
  },
  storeId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Store'
  },
  state: {
    type: Boolean,
    default: true,
  },
});

module.exports = mongoose.model('Section', SectionSchema);