const { Schema, model } = require('mongoose');
const mongoose = require('mongoose');

const RoleSchema = Schema({
    rol: {
        type: String,
        required: [true, 'El rol es obligatorio']
    }
});

module.exports = mongoose.model( 'Role', RoleSchema );
