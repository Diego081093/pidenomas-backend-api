const { Schema, model } = require('mongoose');
const mongoose = require('mongoose');

const ProductSchema = Schema({
    name: {
        type: String,
        required: [true, 'The name is required.'],
    },
    description: {
        type: String,
        required: [true, 'The description is required.'],
    },
    image: {
        type: String,
        required: [true, 'The image is required.'],
    },
    price: {
        type: Number,
        required: [true, 'The price is required.'],
    },
    quantity: {
        type: Number,
        required: [true, 'The quantity is required.'],
    },
    state: {
        type: Boolean,
        default: true,
    },
    public: {
        type: Boolean,
        default: false,
    },
}, {
    timestamps: true,
});

module.exports = mongoose.model( 'Product', ProductSchema );
