const { Schema, model } = require('mongoose');
const mongoose = require('mongoose');

const OfferTaxiSchema = Schema({
  from: {
    longitude: {
      type: String,
      require: [true, "The initial longitude is required"]
    },
    latitude: {
      type: String,
      require: [true, "The initial latitude is required"]
    },
    address: {
      type: String,
      require: [true, "The address is required"]
    }
  },
  to: {
    longitude: {
      type: String,
      require: [true, "The destiny longitude is required"]
    },
    latitude: {
      type: String,
      require: [true, "The destiny latitude is required"]
    },
    address: {
      type: String,
      require: [true, "The address is required"]
    }
  },
  paymentMethod: {
    type: Number,
    require: [true, "The method payment is required"]
  },
  priceService: {
    type: Number,
    require: [true, "The method payment is required"]
  },
  offerResponse: {
    type: [mongoose.Schema.Types.ObjectId],
    default: [],
    ref: 'OfferResponse'
  },
  passenger: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    require: [true, "The passenger is required"]
  },
  state: {
    type: Boolean,
    default: true,
  }
}, {
  timestamps: true,
});

module.exports = mongoose.model('OfferTaxi', OfferTaxiSchema);