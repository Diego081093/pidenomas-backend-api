const { Schema, model } = require('mongoose');
const mongoose = require('mongoose');

const AddressSchema = Schema({
  addressName: {
    type: String,
    required: [true, 'The addressname is required'],
  },
  latitude: {
    type: Number,
    required: [true, 'The latitude is required'],
  },
  longitude: {
    type: Number,
    required: [true, 'The longitude is required'],
  },
  typeHome: {
    type: String,
  },
  reference: {
    type: String,
  },
  idUser: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
}, {
  timestamps: true,
});

module.exports = mongoose.model( 'Address', AddressSchema );