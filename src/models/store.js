const { Schema } = require('mongoose');
const mongoose = require('mongoose');

const StoreSchema = Schema({
  ruc: {
    type: String,
    unique : true,
    required: [true, "The ruc is required."],
  },
  phone: {
    type: String,
    required: [true, "The phone is required"],
  },
  businessName: {
    type: String,
    required: [true, "The businessName is required."],
  },
  address: {
    type: String,
    required: [true, "The address is required."],
  },
  city: {
    type: String,
    required: [true, "The city is required"],
  },
  district: {
    type: String,
    required: [true, "The district is required"],
  },
  logo: {
    type: String,
    required: [true, "The logo is required."],
  },
  frontPage: {
    type: String,
  },
  state: {
    type: Boolean,
    default: false,
  },
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: [true, "This store must be assigned to a user"],
  },
  isOpen: {
    type: Boolean,
    default: false,
  },
  sections: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: 'Section',
  }
});

module.exports = mongoose.model('Store', StoreSchema);