const {Router} = require('express');
const {
  categoryPost, 
  updateCategory, 
  deleteCategory, 
  listCategories, 
  uploadImageCategory
} = require('../controllers/categoryController');
const router = Router();

router.get('/', listCategories);
router.post('/', categoryPost);
router.put('/:id', updateCategory);
router.delete('/:id', deleteCategory);

router.post('/upload', uploadImageCategory);

module.exports = router;