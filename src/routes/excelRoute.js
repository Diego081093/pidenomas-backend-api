const { Router } = require('express');
const multer = require('multer');
const storage = multer.diskStorage({
    destination: 'public/',
    filename: function(req, file, cb) {
      cb(null, 'prueba.xlsx');
    },
  });
const upload = multer({
    storage: storage
})


const { excelPost } = require('../controllers/excelController');

const router = Router();

router.post('/',upload.single('file'), excelPost );


module.exports = router;