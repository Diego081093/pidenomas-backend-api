const { Router } = require('express');

const { createReport, getReports } = require('../controllers/reportController');

const router = Router();

router.post('/', createReport );
router.get('/', getReports );

module.exports = router;