const { Router } = require('express');

const { createRealState, realStateGet, getRealStateById, updateRealState } = require('../controllers/realStateController');

const router = Router();

router.post('/', createRealState );
router.put('/:idRealEstate', updateRealState );
router.get('/', realStateGet );
// router.delete('/:id', deleteProduct );
router.get('/:idUser', getRealStateById);

module.exports = router;