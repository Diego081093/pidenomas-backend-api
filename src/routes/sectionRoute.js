const { Router } = require('express');
const {
  createSection,
  editSection,
  getSectionById
} = require('../controllers/sectionController');
const router = Router();

router.get('/:id', getSectionById);
router.post('/', createSection);
router.put('/:_id', editSection);

module.exports = router;