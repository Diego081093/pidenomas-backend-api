const { Router } = require('express');
const { 
  getChat,
  createChatMessage
} = require('../controllers/chatController');

const router = Router();

router.get('/:idChat', getChat);
router.post('/message', createChatMessage);
module.exports = router;