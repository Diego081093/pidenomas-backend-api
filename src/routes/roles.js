const { Router } = require('express');


const { rolesPost } = require('../controllers/roles');

const router = Router();

router.post('/', rolesPost );


module.exports = router;