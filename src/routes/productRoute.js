const { Router } = require('express');

const { getProductById, createProduct, productsGet, deleteProduct, updateProduct } = require('../controllers/productController');

const router = Router();

router.post('/', createProduct );
router.put('/:id', updateProduct );
router.get('/', productsGet );
router.patch('/:idProduct', deleteProduct );
router.get('/:idProduct', getProductById);

module.exports = router;