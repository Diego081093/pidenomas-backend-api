const { Router } = require('express');

const { createClient, listClient } = require('../controllers/clientController');

const router = Router();

router.post('/', createClient );
router.get('/', listClient );

module.exports = router;