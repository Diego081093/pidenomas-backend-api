const { Router } = require('express');

const { createFile, listFiles } = require('../controllers/filesController');

const router = Router();

router.post('/', createFile );
router.get('/', listFiles );

module.exports = router;