const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');
const { esRoleValido, emailExiste, dniExist } = require('../helpers/db-validators');
const { getUsers,
        createUser,
        loginUser,
        deleteUser,
        changePasswordUser,
        usuariosPatch, 
        updateUser} = require('../controllers/userController');

const router = Router();

router.get('/', getUsers );

router.put('/:id',[
    check('id', 'No es un ID válido').isMongoId(),
], changePasswordUser );

router.post('/', createUser );

router.post('/login', loginUser );
router.post('/:id', updateUser );

router.delete('/:id',[
    check('id', 'No es un ID válido').isMongoId(),
], deleteUser );

//router.patch('/', usuariosPatch );


module.exports = router;