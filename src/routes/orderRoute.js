const { Router } = require('express');

const { getAllOrders, getOrderStore, createOrder, getOrderClient, cancelOrder, setStateOrder } = require('../controllers/orderController');

const router = Router();

router.get('/', getAllOrders);
router.post('/', createOrder );
router.get('/store/:idStore', getOrderStore );
router.get('/client/:idClient', getOrderClient );
router.put('/:idOrder', cancelOrder );
router.put('/:idOrder/state', setStateOrder );

module.exports = router;