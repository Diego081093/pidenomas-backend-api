const { Router } = require('express');

const {
  getStoreById,
  getStoreByOwner,
  listDisabledStores,
  listStores,
  createStore,
  patchStore,
  patchOpenStore,
} = require('../controllers/storeController');

const router = Router();

router.get('/', listStores);
router.get('/disable', listDisabledStores);
router.get('/me/:owner', getStoreByOwner)
router.post('/', createStore);
router.patch('/patch/:idStore', patchStore);
router.patch('/open/:idStore', patchOpenStore);
router.get('/:_id', getStoreById);

module.exports = router;