const { Router } = require('express');
const { 
  createOffer, 
  getOfferTaxi, 
  createTaxi, 
  createResponseOffer,
  updateLocationTaxi,
  updateOfferTaxi,
  getTaxi,
  getOffersResponse,
  cancelTaxi,
} = require('../controllers/taxiController');

const router = Router();

router.get('/', getOfferTaxi);
router.get('/:idTaxi', getTaxi);
router.post('/', createTaxi);
router.post('/offer/', createOffer);
router.post('/response/', createResponseOffer);
router.put('/:idTaxi', updateLocationTaxi);
router.put('/response/:idOffer', updateOfferTaxi);
router.get('/response/:idOffer', getOffersResponse);
router.delete('/cancel/:idTaxi', cancelTaxi);

module.exports = router;