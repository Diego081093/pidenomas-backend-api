const { Router } = require('express');

const { getAddressForUser, createAddress, editAddress, deleteAddress } = require('../controllers/addressController');

const router = Router();

router.get('/:idUser', getAddressForUser);
router.post('/', createAddress);
router.put('/:id', editAddress);
router.delete('/:id', deleteAddress);

module.exports = router;