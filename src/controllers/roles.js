const { response, request } = require('express');

const Role = require('../models/role');



const rolesPost = async(req, res = response) => {
    
    const { rol } = req.body;
    const role = new Role({ rol });

    // Guardar en BD
    await role.save();

    res.json({
        role
    });
}




module.exports = {
    rolesPost
}