const { request, response } = require('express');
const Order = require('../models/order');
const User = require('../models/user');
const Store = require('../models/store');
const Product = require('../models/product');

const getAllOrders = async(request, response) => {
  try {
    let arrayOrders = []
    
    const orders = await Order.find().populate('store');
    if(!orders) {
      response.status(402).json({
        code: 402,
        message: "No found store",
        data: {}
      });
      return;
    }

    const promiseClient = await Promise.all(
      orders.map(async(or) => {
        const client = await User.findById(or.client);
        
        const formatOrder = {
          products: or.products,
          numberPayment: or.numberPayment,
          orderNumber: or.orderNumber,
          screenShotPay: or.screenShotPay,
          state: or.state,
          _id: or._id,
          totalPrice: or.totalPrice,
          addressDelivery: or.addressDelivery,
          store: {
            isOpen: or.store.isOpen,
            _id: or.store._id,
            ruc: or.store.ruc,
            phone: or.store.phone,
            businessName: or.store.businessName,
            address: or.store.address,
            city: or.store.city,
            district: or.store.district,
            logo: or.store.logo,
            owner: {},
          },
          client: {
            _id: client._id,
            img: client.img,
            dni: client.dni,
            birthday: client.birthday,
            name: client.name,
            email: client.email,
            phone: client.phone,
          },
          isCashPayment: or.isCashPayment,
          createdAt: or.createdAt,
        }

        return arrayOrders.push(formatOrder)
      })
    );
  
    response.status(200).json({
      code: 200,
      message: 'Get stores success.',
      data: arrayOrders,
    });
  } catch (e) {
    
    res.status(500).json({
      code: 500,
      message: "Server internal error -> " + e, 
    });
  }
}

const getOrderStore = async(request, response) => {
  try{
    const { idStore } = request.params;
    let orders = {}

    const query = {
      store: idStore,
    };

    new Promise(async function(resolve) {
      const data = await Order.find(query).populate('client');
      if(!data[0]) {
        response.status(402).json({
          code: 402,
          message: "No found client",
          data: []
        });
        reject()
        return;
      }else{
        orders = data
          
        resolve(data)
      }
    }).then(async function(result) {
      const store = await Store.findById(idStore);

      const formatOrder = {
        products: orders[0].products,
        orderNumber: orders[0].orderNumber,
        numberPayment: orders[0].numberPayment,
        screenShotPay: orders[0].screenShotPay,
        state: orders[0].state,
        _id: orders[0]._id,
        totalPrice: orders[0].totalPrice,
        addressDelivery: orders[0].addressDelivery,
        store: {
          isOpen: store.isOpen,
          _id: store._id,
          ruc: store.ruc,
          phone: store.phone,
          businessName: store.businessName,
          address: store.address,
          city: store.city,
          district: store.district,
          logo: store.logo,
          owner: {},
        },
        client: {
          _id: orders[0].client._id,
          img: orders[0].client.img,
          dni: orders[0].client.dni,
          birthday: orders[0].client.birthday,
          name: orders[0].client.name,
          email: orders[0].client.email,
          phone: orders[0].client.phone,
        },
        isCashPayment: orders[0].isCashPayment,
        createdAt: orders[0].createdAt,
      }

      response.status(200).json({
        code: 200,
        message: 'Get order success.',
        data: [formatOrder],
      });
    })
  }catch(e) {
    
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e, 
    });
  }
};

const getOrderClient = async(request, response) => {
  try{
    const { idClient } = request.params;
    let orders = {}

    const query = {
      client: idClient,
    };

    new Promise(async function(resolve) {
      const data = await Order.find(query).populate('store');
      if(!data[0]) {
        response.status(402).json({
          code: 402,
          message: "No found store",
          data: {}
        });
        reject()
        return;
      }else{ 
        orders = data
        
        resolve(data)
      }
    }).then(async function(result) {
      
      const client = await User.findById(orders[0].client);

      const formatOrder = {
        products: orders[0].products,
        orderNumber: orders[0].orderNumber,
        numberPayment: orders[0].numberPayment,
        screenShotPay: orders[0].screenShotPay,
        state: orders[0].state,
        _id: orders[0]._id,
        totalPrice: orders[0].totalPrice,
        addressDelivery: orders[0].addressDelivery,
        store: {
          isOpen: orders[0].store.isOpen,
          _id: orders[0].store._id,
          ruc: orders[0].store.ruc,
          phone: orders[0].store.phone,
          businessName: orders[0].store.businessName,
          address: orders[0].store.address,
          city: orders[0].store.city,
          district: orders[0].store.district,
          logo: orders[0].store.logo,
          owner: {},
        },
        client: {
          _id: client._id,
          img: client.img,
          dni: client.dni,
          birthday: client.birthday,
          name: client.name,
          email: client.email,
          phone: client.phone,
        },
        isCashPayment: orders[0].isCashPayment,
        createdAt: orders[0].createdAt,
      }

      
  
      response.status(200).json({
        code: 200,
        message: 'Get order success.',
        data: formatOrder,
      });
    })
  }catch(e) {
    
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e, 
    });
  }
};

const createOrder = async(request, response) => {
  try {
    const {products, totalPrice, addressDelivery, client, store, isCashPayment, numberPayment, screenShotPay} = request.body

    let orders = {}

    const order = new Order({
      products,
      totalPrice,
      addressDelivery,
      client,
      store,
      isCashPayment,
      numberPayment,
      screenShotPay
    });

    const responseSaveOrder = await order.save();

    new Promise(async function(resolve) {
      const data = await Order.findById(responseSaveOrder._id).populate('store');
      if(!data) {
        response.status(402).json({
          code: 402,
          message: "No found store",
          data: {}
        });
        reject()
        return;
      }else{ 
        orders = data
        
        resolve(data)
      }
    }).then(async function(result) {
      
      const client = await User.findById(orders.client).populate('owner');

      const formatOrder = {
        products: orders.products,
        numberPayment: orders.numberPayment,
        screenShotPay: orders.screenShotPay,
        state: orders.state,
        _id: orders._id,
        totalPrice: orders.totalPrice,
        addressDelivery: orders.addressDelivery,
        store: {
          isOpen: orders.store.isOpen,
          _id: orders.store._id,
          ruc: orders.store.ruc,
          phone: orders.store.phone,
          businessName: orders.store.businessName,
          address: orders.store.address,
          city: orders.store.city,
          district: orders.store.district,
          logo: orders.store.logo,
          owner: {},
        },
        client: {
          _id: client._id,
          img: client.img,
          dni: client.dni,
          birthday: client.birthday,
          name: client.name,
          email: client.email,
          phone: client.phone,
        },
        isCashPayment: orders.isCashPayment,
        createdAt: orders.createdAt,
      }

      //await Promise.all(
      //  formatOrder.products.map(async(p) => {
      //    await Product.findByIdAndUpdate(p, {quantity:})
      //  })
      //)

      
  
      response.status(200).json({
        code: 200,
        message: 'Order created',
        data: formatOrder,
      });
    })
  } catch(e) {
    
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    });
  }
};

const cancelOrder = async(request, response) => {
  try{
    const { idOrder } = request.params;

    await Order.findByIdAndUpdate(idOrder, {state: 0});

    response.status(200).json({
      code: 200,
      message: 'Order is canceled'
    });
  }catch(e) {
    
    res.status(500).json({
      code: 500,
      message: "Server internal error -> " + e, 
    });
  }
};

const setStateOrder = async(request, response) => {
  try{
    const { idOrder } = request.params;
    const { numberState } = request.body;

    new Promise(async function(resolve) {
      const data = await Order.findByIdAndUpdate(idOrder, {state: numberState});
      resolve(data)
    }).then(async function(result) {
      const order = await Order.findById(idOrder);

      response.status(200).json({
        code: 200,
        message: 'State updated',
        data: {
          state: order.state,
        }
      });
    })
  }catch(e) {
    
    res.status(500).json({
      code: 500,
      message: "Server internal error -> " + e, 
    });
  }
};

module.exports = {
  getAllOrders,
  getOrderStore,
  getOrderClient,
  createOrder,
  cancelOrder,
  setStateOrder
}
