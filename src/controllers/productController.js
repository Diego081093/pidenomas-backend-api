const { response, request } = require('express');
const Product = require('../models/product');
const {getStorage, ref} = require('firebase/storage');

const getProductById = async(request, response) => {
    const {idProduct} = request.params;

    const product = await Product.findById(idProduct)

    if(!product) {
        response.json({
            message: 'No se encontro un producto',
            code: 402,
            data: {}
        });
    }

    response.json({
        message: 'Listado de Productos Correctamente',
        code: 200,
        data: product
    });
}

const createProduct = async(req = request, res = response) => {
    try {
        const {idCategory, name, description, image, price, quantity} = req.body;
        const product = new Product({ idCategory, name, description, image, price, quantity });

        const responseSaveProduct =  await product.save();

        res.status(201).json({
            code: 201,
            message: 'Product created',
            data: responseSaveProduct,
        });
    } catch (e) {
        res.status(500).json({
            code: 500,
            message: "Server internal error -> " + e,
        });
    }
}

const productsGet = async(req = request, res = response) => {
    const { limit = 0, offset = 0 } = req.query;
    const query = { state: true };

    const [ total, products ] = await Promise.all([
        Product.countDocuments(query),
        Product.find(query)
        .sort({'createdAt': 1})
        .skip(Number( offset ))
        .limit(Number( limit ))
    ]);

    res.json({
        message: 'Listado de Productos Correctamente',
        code: 200,
        data: products
    });
}

const deleteProduct = async(req, res = response) => {
    try {
        const { idProduct } = req.params;

       await Product.findByIdAndDelete(idProduct);
    
        res.status(200).json({
            code: 200,
            message: 'Product delete',
            data: {}
        });
    } catch (e) {
        res.status(500).json({
            code: 500,
            message: "Server internal error -> " + e,
        });
    }
}

const updateProduct = async(req = request, res = response) => {
    try {
        const { id } = req.params;
        const product = await Product.findById(id);
        await product.update(req.body);
        const updateProduct = await Product.findById(id);

        res.status(200).json({
            code: 200,
            message: 'Product updated',
            data: updateProduct
        });
    } catch (e) {
        res.status(500).json({
            code: 500,
            message: "Server internal error -> " + e,
        });
    }
}


module.exports = {
    getProductById,
    createProduct,
    productsGet,
    deleteProduct,
    updateProduct
}