const { request, response } = require('express');
const Store = require('../models/store');
const Section = require('../models/sections');

const getStoreById = async(request, response) => {
  try {
    const { _id } = request.params;

    let listStore = [];

    const query = {_id: _id};

    const stores = await Store.find(query).populate("sections");

    if(!stores) {
      response.status(402).json({
        code: 402,
        message: "No found store",
        data: {}
      });
      return;
    }

    const promiseStores = await Promise.all(
      stores.map(async(s) => {
        const sections = await Section.find({storeId: s._id}).populate('products');

        const obj = {
          state: s.state,
          isOpen: s.isOpen,
          sections: sections,
          _id: s._id,
          ruc: s.ruc,
          phone: s.phone,
          businessName: s.businessName,
          address: s.address,
          city: s.city,
          district: s.district,
          logo: s.logo,
          frontPage: s.frontPage,
          owner: {
            img: s.owner.img,
            dni: s.owner.dni,
            birthday: s.owner.birthday,
            state: s.owner.state,
            google: s.owner.google,
            _id: s.owner._id,
            name: s.owner.name,
            email: s.owner.email,
            phone: s.owner.phone,
            gender: s.owner.gender,
            rol: s.owner.rol,
            createdAt: s.owner.createdAt,
            updatedAt: s.owner.updatedAt
          },
          __v: s.__v
        };

        return listStore.push(obj);
      })
    );

    response.status(200).json({
      code: 200,
      message: "Success",
      data: listStore[0]
    });
  } catch(e) {
  
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    });
  }
};

const getStoreByOwner = async(request, response) => {
  console.log("Entro a la funcion");
  try {
    const { owner } = request.params;

    let listStore = [];

    const query = {owner: owner};

    const stores = await Store.find(query).populate("sections");

    console.log("-->", stores);

    if(stores.length === 0) {
      response.status(402).json({
        code: 402,
        message: "Not found store",
      });
      return;
    }

    const promiseStores = await Promise.all(
      stores.map(async(s) => {
        const sections = await Section.find({storeId: s._id}).populate('products');

        const obj = {
          state: s.state,
          isOpen: s.isOpen,
          sections: sections,
          _id: s._id,
          ruc: s.ruc,
          phone: s.phone,
          businessName: s.businessName,
          address: s.address,
          city: s.city,
          district: s.district,
          logo: s.logo,
          frontPage: s.frontPage,
          owner: {
            img: s.owner.img,
            dni: s.owner.dni,
            birthday: s.owner.birthday,
            state: s.owner.state,
            google: s.owner.google,
            _id: s.owner._id,
            name: s.owner.name,
            email: s.owner.email,
            phone: s.owner.phone,
            gender: s.owner.gender,
            rol: s.owner.rol,
            createdAt: s.owner.createdAt,
            updatedAt: s.owner.updatedAt
          },
          __v: s.__v
        };

        return listStore.push(obj);
      })
    );

    response.status(200).json({
      code: 200,
      message: "Success",
      data: listStore[0],
    });
  } catch(e) {
  
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    });
  }
}

const listDisabledStores = async(request, response) => {
  try {
    let listStores = [];

    const query = {state: false};
    const { limit = 0, from = 0 } = request.query;
    
    const stores = await Store.find(query)
    .skip( Number( from ) )
    .limit(Number( limit ))
    .populate('owner');

    const promiseStores = await Promise.all(
      stores.map(async(s) => {

      

        const sections = await Section.find({storeId: s._id}).populate('products');
  
        const obj = {
          state: s.state,
          isOpen: s.isOpen,
          sections: sections,
          _id: s._id,
          ruc: s.ruc,
          phone: s.phone,
          businessName: s.businessName,
          address: s.address,
          city: s.city,
          district: s.district,
          logo: s.logo,
          frontPage: s.frontPage,
          owner: {
            img: s.owner.img,
            dni: s.owner.dni,
            birthday: s.owner.birthday,
            state: s.owner.state,
            google: s.owner.google,
            _id: s.owner._id,
            name: s.owner.name,
            email: s.owner.email,
            phone: s.owner.phone,
            gender: s.owner.gender,
            rol: s.owner.rol,
            createdAt: s.owner.createdAt,
            updatedAt: s.owner.updatedAt
          },
          __v: s.__v
        };

        return listStores.push(obj);
      })
    );

    response.status(200).json({
      code: 200,
      message: 'Get stores success.',
      data: listStores,
    });
  } catch (e) {
  
    res.status(500).json({
      code: 500,
      message: "Server internal error -> " + e, 
    });
  }
}

const listStores = async(request, response) => {
  try {
    let listStores = [];

    const query = {state: true};
    const { limit = 0, from = 0 } = request.query;
    
    const stores = await Store.find(query)
    .skip( Number( from ) )
    .limit(Number( limit ))
    .populate('owner');

    const promiseStores = await Promise.all(
      stores.map(async(s) => {

      

        const sections = await Section.find({storeId: s._id}).populate('products');
  
        const obj = {
          state: s.state,
          isOpen: s.isOpen,
          sections: sections,
          _id: s._id,
          ruc: s.ruc,
          phone: s.phone,
          businessName: s.businessName,
          address: s.address,
          city: s.city,
          district: s.district,
          logo: s.logo,
          frontPage: s.frontPage,
          owner: {
            img: s.owner.img,
            dni: s.owner.dni,
            birthday: s.owner.birthday,
            state: s.owner.state,
            google: s.owner.google,
            _id: s.owner._id,
            name: s.owner.name,
            email: s.owner.email,
            phone: s.owner.phone,
            gender: s.owner.gender,
            rol: s.owner.rol,
            createdAt: s.owner.createdAt,
            updatedAt: s.owner.updatedAt
          },
        };

        return listStores.push(obj);
      })
    );

    response.status(200).json({
      code: 200,
      message: 'Get stores success.',
      data: listStores,
    });
  } catch (e) {
  
    res.status(500).json({
      code: 500,
      message: "Server internal error -> " + e, 
    });
  }
}

const createStore = async(request, response) => {
  try {
    const {
      ruc, 
      phone, 
      businessName, 
      address, 
      city, 
      district, 
      logo, 
      frontPage, 
      owner
    } = request.body;

    const existRuc = await Store.findOne({ ruc: ruc })

    if(existRuc) {
      response.status(400).json({
        code: 400,
        message: 'Ruc already exist',
      });

      return;
    }

    const store = new Store({
      ruc,
      phone,
      businessName,
      address,
      city,
      district,
      logo,
      frontPage,
      owner
    });

    const responseSaveStore = await store.save();

    response.status(201).json({
      code: 201,
      message: 'Store created',
      data: responseSaveStore,
    });
  } catch (e) {
  
    response.status(500).json({
        code: 500,
        message: "Server internal error -> " + e,
    });
  }
}

const patchStore = async(request, response) => {
  try {
    const { idStore } = request.params;

    const store = await Store.findById(idStore);

    await Store.findByIdAndUpdate(idStore, {state: !store.state});

    response.status(200).json({
      code: 400,
      message: 'Store state is update',
    });

  } catch (e) {
  
    response.status(500).json({
        code: 500,
        message: "Server internal error -> " + e,
    });
  }
}

const patchOpenStore = async(request, response) => {
  try {
    const { idStore } = request.params;

    const store = await Store.findById(idStore);

    await Store.findByIdAndUpdate(idStore, {isOpen: !store.isOpen});

    if(store.isOpen == true) {
      response.status(200).json({
        code: 200,
        message: 'Store is close',
      });
    }else {
      response.status(200).json({
        code: 200,
        message: 'Store is open',
      });
    }
  } catch (e) {
    response.status(500).json({
        code: 500,
        message: "Server internal error -> " + e,
    });
  }
}

module.exports = {
  getStoreById,
  getStoreByOwner,
  listDisabledStores,
  listStores,
  createStore,
  patchStore,
  patchOpenStore,
}