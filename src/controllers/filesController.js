const { response, request } = require('express');
const File = require('../models/files');

const createFile = async(req = request, res = response) => {
    try {
        const file = new File(req.body);

        const responseSaveFile =  await file.save();

        res.status(200).json({
            code: 200,
            message: 'File created',
            data: responseSaveFile
        });
    } catch (e) {
        console.log(e);
        res.status(500).json({
            code: 500,
            message: "Server internal error -> " + e,
        });
    }
}


const listFiles = async(req = request, res = response) => {

    const { limit = 5, offset = 0 } = req.query;
    const query = { state: true };

    const [ total, files ] = await Promise.all([
        File.countDocuments(query),
        File.find(query)
            .skip( Number( offset ) )
            .limit(Number( limit )).
            populate('Store')
    ]);

    res.json({
        message: 'Listado de Clientes Correctamente',
        code: 200,
        data: files
    });
}



module.exports = {
    createFile,
    listFiles
}