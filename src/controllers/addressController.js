const { response, request } = require('express');
const Address = require('../models/address');

const getAddressForUser = async(request, response) => {
  try {
    const { idUser } = request.params;
    const query = { idUser: idUser };

    const addresses = await Address.find(query);

    response.status(200).json({
      message: 'Listado de direcciones',
      code: 200,
      data: addresses,
  });
  } catch (e) {
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    });
  }
};

const createAddress = async(req = request, res = response) => {
  try {
    const { addressName, latitude, longitude, typeHome, reference, idUser } = req.body;
    const address = new Address({ addressName, latitude, longitude, typeHome, reference, idUser });

    const responseSaveAddress = await address.save();

    res.status(201).json({
      code: 201,
      message: 'Address created',
      data: responseSaveAddress,
    });
  } catch (e) {
    res.status(500).json({
        code: 500,
          message: "Server internal error -> " + e,
    });
  }
};

const editAddress = async(req = request, res = response) => {
  try {
    const { id } = req.params;
    const { addressName, latitude, longitude, typeHome, reference, idUser, ...rest } = req.body;

    const address = Address.findById(id);

    if(!address) {
      res.status(402).json({
        code:402,
        message: 'No se encontro el item',
      });
      return;
    }

    rest.idUser = idUser;
    rest.addressName = addressName ? addressName : address.addressName;
    rest.latitude = latitude ? latitude : address.latitude;
    rest.longitude = longitude ? longitude : address.longitude;
    rest.typeHome = typeHome ? typeHome : address.typeHome;
    rest.reference = reference ? reference : address.reference;

    await Address.findByIdAndUpdate(id, rest);

    res.json({
      code: 200,
      message: 'Edit success',
    });
  } catch (e) {
    res.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    });
  }
};

const deleteAddress = async(request, response) => {
  try {
    const { id } = request.params;

    await Address.findByIdAndDelete(id);

    response.status(200).json({
      code: 200,
      message: "Delete success",
    });
  } catch (e) {
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    });
  }
}

module.exports = {
  getAddressForUser,
  createAddress,
  editAddress,
  deleteAddress,
}