const { response, request } = require('express');
const RealState = require('../models/realState');
const {getStorage, ref} = require('firebase/storage');

const getRealStateById = async(request, response) => {
    const {idUser} = request.params;

    const realState = await RealState.find({ user: idUser }).populate("user")

    if(!realState) {
        response.json({
            message: 'No se encontro un producto',
            code: 402,
            data: {}
        });
    }

    response.json({
        message: 'Listado de Inmobiliaria Correctamente',
        code: 200,
        data: realState
    });
}

const createRealState = async(req = request, res = response) => {
    try {
        const realState = new RealState(req.body);

        const responseSaveRealState =  await realState.save();

        res.status(200).json({
            code: 200,
            message: 'Real State created',
            data: responseSaveRealState,
        });
    } catch (e) {
        res.status(500).json({
            code: 500,
            message: "Server internal error -> " + e,
        });
    }
}

const realStateGet = async(req = request, res = response) => {
    const { limit = 0, offset = 0 } = req.query;
    const query = { state: true };

    const [ total, realStates ] = await Promise.all([
        RealState.countDocuments(query),
        RealState.find(query)
        .populate('user')
        .sort({'createdAt': 1})
        .skip(Number( offset ))
        .limit(Number( limit ))
    ]);

    res.json({
        message: 'Listado de Inmobiliarias Correctamente',
        code: 200,
        data: realStates
    });
}
// const deleteProduct = async(req, res = response) => {
//     try {
//         const { id } = req.params;

//         const product = await Product.findByIdAndUpdate( id, { state: false } );
    
//         res.json(product);   
//     } catch (e) {
//         console.log(e);
//         res.status(500).json({
//             code: 500,
//             message: "Server internal error -> " + e,
//         });
//     }
// }
const updateRealState = async(request, response) => {
    try {
        const { idRealEstate } = request.params;
        const realState = await RealState.findById(idRealEstate);
        await realState.update(request.body);
        const updateRealState = await RealState.findById(idRealEstate);

        response.status(200).json({
            code: 200,
            message: 'RealState updated',
            data: updateRealState
        });
    } catch (e) {
        response.status(500).json({
            code: 500,
            message: "Server internal error -> " + e,
        });
    }
}


module.exports = {
    getRealStateById,
    createRealState,
    realStateGet,
    // deleteProduct,
    updateRealState
}