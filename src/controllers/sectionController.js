const {request, response} = require('express');
const Section = require('../models/sections');

const createSection = async(request, response) => {
  try {
    const {name, products, storeId} = request.body;

    const section = new Section({
      name,
      products,
      storeId,
    });

    const responseSaveSection = await section.save();

    response.status(201).json({
      code: 201,
      message: 'Section created',
      data: responseSaveSection,
    });
  } catch(e) {
    console.log(e);
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    });
  }
};

const editSection = async(request, response) => {
  try{
    const { _id } = request.params;
    const {name, product, ...rest} = request.body;

    if(name === "") {
      response.status(400).json({
        code: 400,
        message: "The name section is required",
      });
      return;
    }

    if(!product) {
      response.status(400).json({
        code: 400,
        message: "The products is required",
      });
      return;
    }

    const prevSection = await Section.findById(_id);

    rest.name = name? name : prevSection.name;

    const productsArray = prevSection.products;
    productsArray.push(product);

    rest.products = productsArray;

    await Section.findByIdAndUpdate(_id, rest);

    const section = await Section.findById(_id).populate("products");

    response.status(200).json({
      code: 200,
      data: section,
      message: "Update section success.",
    });
  }catch(e) {
    console.log(e);
    res.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    });
  }
};

const getSectionById = async(request, response) => {
  try {
    const { id } = request.params;

    const section = await Section.findById(id).populate("products");

    if(!section) {
      response.status(402).json({
        code: 402,
        message: "Not found section"
      });
      return;
    }

    response.status(200).json({
      code: 200,
      message: "Success",
      data: section,
    });
  } catch (e) {
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    });
  }
}

module.exports = { createSection, editSection, getSectionById };