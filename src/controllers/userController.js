const { response, request } = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken')
const User = require('../models/user');
const Taxi = require('../models/taxi');
const { body } = require('express-validator');

const getUsers = async(req = request, res = response) => {
    try {
        const { limit = 0, from = 0 } = req.query;
        const query = { state: true };

        const [ total, users ] = await Promise.all([
            User.countDocuments(query),
            User.find(query)
                .skip( Number( from ) )
                .limit(Number( limit ))
                .populate('rol')
        ]);

        res.status(200).json({
            code: 200,
            data: users,
            message: 'Success',
        });
    } catch (e) {
        
        res.status(500).json({
            code: 500,
            message: "Server internal error -> " + e,
        });
    }
}

const createUser = async(req = request, res = response) => {
    try {
        const { name, email, password, dni, phone, gender, birthday, rol } = req.body;

        

        await User.findOne({ dni: dni }, async(err, dataOne) => {
            if(dataOne){
                res.status(402).json({
                    code: 402,
                    message: 'El DNI ya esta en uso',
                    data: {}
                });
                return;
            }

            await User.findOne({ email: email }, async(err, dataTwo) => {
                if(dataTwo) {
                    res.status(402).json({
                        code: 402,
                        message: 'El correo ya esta en uso',
                        data: {}
                    });
                    return;
                }

                if(password.length < 6) {
                    res.status(402).json({
                        code: 402,
                        message: 'La contraseña debe tener al menos 6 digitos',
                        data: {}
                    });
                    return;
                }
    
                const user = new User({ name, email, password, dni, phone, gender, birthday, rol });
    
                // Encriptar la contraseña
                const salt = bcrypt.genSaltSync();
                user.password = bcrypt.hashSync( password, salt );
    
                // Guardar en BD
                await user.save();
                const token = await jwt.sign({user: user.toJSON()}, 'pidenomas_token');
    
                res.status(201).json({
                    code: 201,
                    message: 'Register successfull',
                    data: {
                        token: token,
                    }
                });
            });
        });
    } catch (e) {
        
        res.status(500).json({
            code: 500,
            message: "Server internal error -> " + e,
        });
    }
}

const loginUser = async(req = request, res = response) => {
    try{
        const { email, password } = req.body;
        
        const user = await User.findOne({ email });

        if(!user){
            res.status(402).json({
                code:402,
                message: 'Credenciales incorrectas',
                data: {}
            });
            return;
        }

        let exitsTaxi = {};

        const promise = new Promise(async function(resolve) {
            const data = await await Taxi.findOne({driver: user._id});
            exitsTaxi = data;
            resolve(exitsTaxi);
        })

        promise.then(async(result) => {
            const formatUser = {
                img: user.img,
                dni: user.dni,
                birthday: user.birthday,
                state: user.state,
                _id: user._id,
                name: user.name,
                email: user.email,
                phone: user.phone,
                gender: user.gender,
                rol: user.rol,
                haveTaxi: exitsTaxi? exitsTaxi._id : "",
                createdAt: user.createdAt,
                updatedAt: user.updatedAt
            }

            if (await bcrypt.compare(password, user.password)) {
                const token = await jwt.sign({user: formatUser}, 'pidenomas_token', {expiresIn: "1d"});

                res.json({
                    code: 200,
                    message: 'Success',
                    data: {
                        token: token,
                    }
                });
            } else {
                res.status(402).json({
                    code:402,
                    message: 'Credenciales incorrectas'
                });
            }
        })
    } catch (e) {
        
        res.status(500).json({
            code: 500,
            message: "Server internal error -> " + e,
        });
    }
}

const changePasswordUser = async(req = request, res = response) => {
    try {
        const { id } = req.params;
        const { password, newPassword, ...rest } = req.body;

        const user = await User.findOne({ id });

        if(password != "" && newPassword != ""){
            const salt = bcrypt.genSaltSync();
            const passwordDecoded = bcrypt.hashSync( password, salt );
            const passwordCheck = bcrypt.compare(passwordDecoded, user.password);

            if(passwordCheck) {
                const salt = bcrypt.genSaltSync();
                rest.password = bcrypt.hashSync( newPassword, salt );

                await user.findByIdAndUpdate(id, rest);

                res.status(200).json({
                    code: 200,
                    message: 'Password updated',
                });
            } else {
                res.status(400).json({
                    code: 400,
                    message: 'Incorrect password',
                });
            }
        } else {
            res.status(400).json({
                code: 400,
                message: 'password and new password are required',
            });
        }
    } catch (e) {
        
        res.status(500).json({
            code: 500,
            message: "Server internal error -> " + e,
        });
    }
}

const usuariosPatch = (req, res = response) => {
    res.json({
        msg: 'patch API - usuariosPatch'
    });
}
const updateUser = async(req = request, res = response) => {
    try {
        const { id } = req.params;
        const user = await User.findById(id);
        await user.update(req.body);

        res.status(200).json({
            code: 200,
            message: 'User updated'
        });
    } catch (e) {
        
        res.status(500).json({
            code: 500,
            message: "Server internal error -> " + e,
        });
    }
}
const deleteUser = async(req, res = response) => {
    try {
        const { id } = req.params;

        const user = await User.findByIdAndUpdate( id, { state: false } );
    
        res.json(user);   
    } catch (e) {
        
        res.status(500).json({
            code: 500,
            message: "Server internal error -> " + e,
        });
    }
}

module.exports = {
    getUsers,
    createUser,
    loginUser,
    changePasswordUser,
    usuariosPatch,
    deleteUser,
    updateUser
}