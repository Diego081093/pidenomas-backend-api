const { response, request } = require('express');
const { getStorage, ref, uploadBytes } = require('firebase/storage');
const Category = require('../models/category');

const listCategories = async(req = request, res = response) => {
    try {
        const { limit = 20, offset = 0 } = req.query;
        const query = { state: true };

        const [ total, products ] = await Promise.all([
            Category.countDocuments(query),
            Category.find(query)
                .skip( Number( offset ) )
                .limit(Number( limit ))
        ]);

        res.status(200).json({
            message: 'Get categories success.',
            code: 200,
            data: products
        });
    } catch (e) {
        res.status(500).json({
           code: 500,
           message: "Server internal error -> " + e, 
        });
    }
}

const uploadImageCategory = async(request, response) => {
    const storage = getStorage();
    const storageRef = ref(storage, 'categories');

    const image = request.files;


    await storageRef.put(image).then((snapshot) => {

    });

}

const categoryPost = async(req = request, res = response) => {
    try{
        const { name } = req.body;

        if(name.length < 1){
            res.status(400).json({
                code: 400,
                message: "The name category is required.",
            });
            return;
        }

        var idCat = "C-";
        if(name.length > 1) {
            idCat = idCat + name.toString().substr(0, 1) + Math.floor(Math.random() * 99);
        }else {
            idCat = idCat + name.toString().substr(0, 0) + Math.floor(Math.random() * 99);
        }
        const category = new Category({ idCat, name });

        // Save in DB
        await category.save();

        res.status(200).json({
            code: 200,
            data: category,
            message: "Create category success.",
        });
    }catch(e){

        res.status(500).json({
            code: 500,
            message: "Server internal error -> " + e, 
         });
    }
}

const updateCategory = async(req = request, res = response) => {
    try {
        const { id } = req.params;
        const { name, ...rest } = req.body;

        if(name === ""){
            res.status(400).json({
                code: 400,
                message: "The name category is required.",
            });
            return;
        }

        rest.name = name;

        await Category.findByIdAndUpdate(id, rest);

        const category = await Category.findById(id);

        res.status(200).json({
            code: 200,
            data: category,
            message: "Update category success.",
        });
    } catch (e) {

        res.status(500).json({
            code: 500,
            message: "Server internal error -> " + e,
        });
    }
}

const deleteCategory = async(req = request, res = response) => {
    try {
        const { id } = req.params;

        await Category.findByIdAndUpdate(id, {state: false});

        res.status(200).json({
            code: 200,
            message: "Delete category success.",
        });
    } catch (e) {

        res.status(500).json({
           code: 500,
           message: "Server internal error -> " + e, 
        });
    }
}

module.exports = {
    listCategories,
    categoryPost,
    updateCategory,
    deleteCategory,
    uploadImageCategory,
}