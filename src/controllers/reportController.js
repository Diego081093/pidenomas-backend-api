const { response, request } = require('express');
const Report = require('../models/report');

const createReport = async(request, response) => {
  try {
    const { description, user } = request.body;
    const report = new Report({description, user});

    const createReport = await report.save();

    response.status(201).json({
      code: 201,
      message: "Report create success",
      data: {createReport}
    });
  } catch (e) {
    console.log(e);
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    });
  }
};

const getReports = async(request, response) => {
  try {
    const reports = await Report.find().populate("user");

    if(!reports) {
      response.status(402).json({
        code: 402,
        message: "Not found data",
      });
      return;
    }

    response.status(201).json({
      code: 201,
      message: "Report create success",
      data: reports
    });
  } catch (e) {
    console.log(e);
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    });
  }
};

module.exports = {
  createReport,
  getReports
}