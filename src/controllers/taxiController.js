const { request, response } = require('express');
const OfferTaxi = require('../models/offerTaxi');
const ResponseOffer = require('../models/OfferResponse');
const Taxi = require('../models/taxi');
const OfferResponse = require('../models/OfferResponse');

const createTaxi = async(request, response) => {
  try {
    const {
      brand, 
      model, 
      color, 
      tuition, 
      imageCar, 
      imageCardProperty, 
      imageIdentification, 
      imageCardLicense, 
      imageCardSoat, 
      driver
    } = request.body;

    const taxi = new Taxi({
      carDetails: {
        brand,
        model,
        color, 
        tuition, 
        imageCar, 
        imageCardProperty, 
        imageIdentification, 
        imageCardLicense, 
        imageCardSoat
      },
      driver
    });

    const responseSaveTaxi = await taxi.save();

    let data = {};

    const promise = new Promise(async function(resolve) {
      const fullTaxi = await Taxi
                              .findOne({_id: responseSaveTaxi._id})
                              .populate("driver");

      data = fullTaxi;
      resolve(data);
    })
    
    promise.then(result => {
      response.status(201).json({
        code: 201,
        message: 'Offer sended',
        data: data
      });
    })
  } catch (e) {
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    })
  }
}

const createOffer = async(request, response) => {
  try {
    const {from, to, paymentMethod, priceService, passenger} = request.body;

    const offer = new OfferTaxi({
      from,
      to,
      paymentMethod,
      priceService,
      passenger
    });

    const responseSaveOfferTaxi = await offer.save();

    const data = await OfferTaxi.findById(responseSaveOfferTaxi._id).populate('passenger');

    console.log(data);

    response.status(201).json({
      code: 201,
      message: 'Offer sended',
      data: data
    })
  } catch (e) {
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    })
  }
}

const createResponseOffer = async(request, response) => {
  try {
    const {taxi, priceOffer} = request.body;

    const responseOffer = new ResponseOffer({
      taxi,
      priceOffer
    })

    const responseSaveResponseOffer = await responseOffer.save();

    response.status(201).json({
      code: 201,
      message: 'Create offer',
      data: responseSaveResponseOffer
    })
  } catch(e) {
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    })
  }
};

const getOfferTaxi = async(request, response) => {
  try {
    const offers = await OfferTaxi.find({state: true}).populate('passenger');

    response.status(200).json({
      message: 'Listado de ofertas',
      code: 200,
      data: offers,
    })
  } catch (e) {
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    })
  }
}

const updateLocationTaxi = async(request, response) => {
  try {
    const { idTaxi } = request.params;
    const { longitude, latitude, address } = request.body;

    const dataUpdate = {
      longitude: longitude,
      latitude: latitude,
      address: address
    }

    await Taxi.findByIdAndUpdate(idTaxi, {currentLocation: dataUpdate});

    let dataTaxiUpdate = {};

    const promise = new Promise(async function(resolve) {
      const data = await Taxi.findById(idTaxi).populate("driver");
      dataTaxiUpdate = data;
      resolve(data);
    });

    promise.then(result => {
      response.status(200).json({
        code: 200,
        message: 'Update coordinates',
        data: dataTaxiUpdate
      });
    })
  } catch (e) {
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    });
  }
}

const updateOfferTaxi = async(request, response) => {
  try {
    const { idOfferResponse } = request.body;
    const { idOffer } = request.params;

    const offer = await OfferTaxi.findById(idOffer);

    const offersResponseArray = offer.offerResponse;
    offersResponseArray.push(idOfferResponse);

    await OfferTaxi.findByIdAndUpdate(idOffer, {offerResponse: offersResponseArray});

    response.status(200).json({
      code: 200,
      message: "Update offersResponse success",
      data: {}
    });
  } catch (e) {
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    });
  }
};

const getTaxi = async(request, response) => {
  try {
    const { idTaxi } = request.params;

    const data = await Taxi.findById(idTaxi).populate("driver");

    if(data) {
      response.status(200).json({
        code: 200,
        message: 'Not found data',
      });

      return;
    }

    response.status(200).json({
      code: 200,
      message: 'Get taxi successfull',
      data: data
    });
  } catch (e) {
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    });
  }
}

const getOffersResponse = async(request, response) => {
  try {
    const { idOffer } = request.params;

    let arrayOffer = [];

    const data = await OfferTaxi.findById(idOffer).populate("offerResponse");

    const promiseTaxi = await Promise.all(
      data.offerResponse.map(async(or) => {
        const taxi = await Taxi.findById(or.taxi).populate("driver");

        const formatOffert = {
          _id: or._id,
          taxi: taxi,
          priceOffer: or.priceOffer,
        };

        return arrayOffer.push(formatOffert);
      })
    );

    console.log(arrayOffer);

    response.status(200).json({
      code: 200,
      message: 'Get taxi offers successfull',
      data: arrayOffer
    });
  } catch (e) {
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    });
  }
};

const updateAcceptOffer = async(request, response) => {
  try {
    const {idOffer} = request.body;

  }catch(e) {
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e,
    });
  }
}

const cancelTaxi = async(request, response) => {
  try {
    const {idTaxi} = request.params;

    await Taxi.findByIdAndRemove(idTaxi);

    response.status(200).json({
      code: 200,
      message: 'Taxi is canceled'
    });
  } catch(e) {
    response.status(500).json({
      code: 500,
      message: "Server internal error -> " + e
    });
  }
};

module.exports = {
  createTaxi,
  createOffer,
  createResponseOffer,
  getOfferTaxi,
  updateLocationTaxi,
  updateOfferTaxi,
  getTaxi,
  getOffersResponse,
  cancelTaxi,
}