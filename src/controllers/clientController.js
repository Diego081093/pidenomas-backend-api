const Multer = require('multer');
const { response, request } = require('express');
const Client = require('../models/client');

const createClient = async(req = request, res = response) => {
    try {
        const {name, plan, local, address} = req.body;
        const client = new Client({ name, plan, local, address });

        const responseSaveClient =  await client.save();

        res.status(200).json({
            code: 200,
            message: 'Client created',
            data: responseSaveClient
        });
    } catch (e) {
        res.status(500).json({
            code: 500,
            message: "Server internal error -> " + e,
        });
    }
}


const listClient = async(req = request, res = response) => {

    const { limit = 5, offset = 0 } = req.query;
    const query = { state: true };

    const [ total, clients ] = await Promise.all([
        Product.countDocuments(query),
        Product.find(query)
            .skip( Number( offset ) )
            .limit(Number( limit ))
    ]);

    res.json({
        message: 'Listado de Clientes Correctamente',
        code: 200,
        data: clients
    });
}



module.exports = {
    createClient,
    listClient
}