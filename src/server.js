const express = require('express');
const cors = require('cors');
require ('@babel/polyfill');
const {initSocket, initSocketHttps} = require("./socket");
const axios = require('axios').default;
const { dbConnection } = require('./database/config');
const { firebaseConnect } = require('./database/cloudStorage');
const fileUpload = require('express-fileupload');
const morgan = require('morgan');
const fs = require('fs');
const https = require('https');

class Server {
    constructor() {
        this.app  = express();
        this.port = process.env.PORT;
        this.userPath = '/user';
        this.rolePath = '/role';
        this.productPath = '/product';
        this.categoryPath = '/category';
        this.addressPath = '/address';
        this.storePath = '/store';
        this.sectionPath = '/section';
        this.orderPath = '/order';
        this.reportPath = '/report'
        this.realStatePath = '/realState'
        this.excelPath = '/excel'
        this.filePath = '/file'
        this.taxi = '/taxi'
        this.chatPath = '/chat'

        // Conexion database
        this.conectarDB();

        //FirebaseConfig
        firebaseConnect();

        // Middlewares
        this.middlewares();

        // App routes
        this.routes();
    }

    async conectarDB() {
        await dbConnection();
    }

    middlewares() {
        // CORS
        this.app.use( cors() );

        // Read and parse of body
        this.app.use( express.json() );

        // Public directory
        this.app.use( express.static('public') );

        // Upload images
        this.app.use( fileUpload() );

        // Request logger
        this.app.use( morgan('dev') );
    }

    routes() {
        this.app.use( this.userPath, require('./routes/userRoute'));
        this.app.use( this.rolePath, require('./routes/roles'));
        this.app.use( this.productPath, require('./routes/productRoute'));
        this.app.use( this.categoryPath, require('./routes/categoryRoute'));
        this.app.use( this.addressPath, require('./routes/addressRoute'));
        this.app.use( this.storePath, require('./routes/storeRoute'));
        this.app.use( this.sectionPath, require('./routes/sectionRoute'));
        this.app.use( this.orderPath, require('./routes/orderRoute'));
        this.app.use( this.reportPath, require('./routes/reportRoute'));
        this.app.use( this.realStatePath, require('./routes/realStateRoute'));
        this.app.use( this.excelPath, require('./routes/excelRoute'));
        this.app.use( this.filePath, require('./routes/filesRoute'));
        this.app.use( this.taxi, require('./routes/taxiRoute'))
        this.app.use( this.chatPath, require('./routes/chatRoute'))
    }

    // listen() {
    //     this.app.listen( this.port, () => {
    //         console.log('Servidor corriendo en puerto', this.port );
    //     });
    // }

    async consultApiMapTest() {
        axios.get('https://maps.googleapis.com/maps/api/distancematrix/json?origins=-11.950538,-77.057660&destinations=-11.959057,-77.058635&mode=driving&key=AIzaSyA54web1Mkx218BmRy2vIBGAQFGvr7rZMs')
            .then(function (response) {
                response.data.rows.forEach(global => {
                    console.log(global);
                    global.elements.forEach(item => {
                        console.log(item);
                    });
                })
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    async listen() {
        const app = express();
        const node_port = process.env.PORT
        if(process.env.NODE_ENV === 'development'){
            //develop
            // await app.listen(node_port);
            // console.log('Server on port ' + node_port);
            this.app.listen( this.port, () => {
                console.log('Servidor corriendo en puerto', this.port );
            });
            await initSocket()
        }else{
            //production
            https.createServer({
                ca: fs.readFileSync('cert/ca_bundle.crt', 'utf8'),
                cert: fs.readFileSync('cert/certificate.crt', 'utf8'),
                key: fs.readFileSync('cert/private.key', 'utf8')
            }, this.app).listen(this.port, () => {
                console.log("My HTTPS server listening on port " + this.port + "...");
            });
            await initSocketHttps()
        }
    }    
}

module.exports = Server;
