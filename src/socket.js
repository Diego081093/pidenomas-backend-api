require ('@babel/polyfill');
const dotenv = require('dotenv');
const fs = require('fs');
const https = require('https');
const axios = require('axios').default;
const Taxi = require('./models/taxi');

dotenv.config()

exports.initSocket = async () => {
    const httpServer = require("http").createServer();
    const io = require("socket.io")(httpServer, {
        cors: {
            origin: '*',
        }
    });
    initIO(io)
    httpServer.listen(3000);
};

exports.initSocketHttps = async () => {
    const serverSocket = https.createServer({
        ca: fs.readFileSync('cert/ca_bundle.crt', 'utf8'),
        cert: fs.readFileSync('cert/certificate.crt', 'utf8'),
        key: fs.readFileSync('cert/private.key', 'utf8')
    })
    const options = {
        cors: {
            origin: '*',
        }
    };
    const io = require("socket.io")(serverSocket, options);
    initIO(io)
    serverSocket.listen(process.env.SOCKET_PORT);
};

function initIO(io) {
    io.on("connection", socket => {
        console.log('connected: ' + socket.id)

        socket.on("fromUserRequest", async(data) => {
            const datos = {
                message: "Necesario actualizar la lista de ofertas."
            }
            
            const baseUrl = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='

            const listTaxis = await Taxi.find();

            listTaxis.forEach((item) => {
                console.log(data.latlng + " - " + item.currentLocation.latitude + "," + item.currentLocation.longitude);
                axios.get(baseUrl + data.latlng + '&destinations=' + item.currentLocation.latitude + "," + item.currentLocation.longitude + '&mode=driving&key=' + process.env.GOOGLE_MAPS_API_KEY)
                    .then(function (response) {
                        console.log(response.data.rows[0].elements);
                        if (response.data.rows.length > 0) {
                            response.data.rows.forEach(global => {
                                if (global.elements.length > 0) {
                                    global.elements.forEach(element => {
                                        if(element.distance.value < 1500) {
                                            console.log(`updateList${item._id}`)
                                            io.emit(`updateList${item._id}`, datos);
                                        } else {
                                            return
                                        }
                                    });
                                }else {
                                    return
                                }
                            })
                        }else {
                            return
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                        return false
                    })
            })
        });

        socket.on("sendResponseOffer", async(data) => {
            io.emit(`existOffer${data.idOffer}`)
        })
        
    })
    console.log('Socket on port 3000');
};