const Role = require('../models/role');
const User = require('../models/user');

const esRoleValido = async(rol = '') => {

    const rolExist = await Role.findOne({ rol });
    if ( !rolExist ) {
        throw new Error(`El rol ${ rol } no está registrado en la BD`);
    }
}

const emailExiste = async(email = '') => {

    // Verificar si el correo existe
    const emailExist = await User.findOne({ email });
    if ( emailExist ) {
        throw new Error(`This email: ${ email }, already exists`);
    }
}

const dniExist = async(dni) => {
    const existDni = await User.findOne({dni});
    if(existDni) {
        throw new Error(`This dni: ${dni}, already exists`)
    }
}

const existeUsuarioPorId = async( id ) => {

    // Verificar si el correo existe
    const userExist = await User.findById(id);
    if ( !userExist ) {
        throw new Error(`El id no existe ${ id }`);
    }
}



module.exports = {
    esRoleValido,
    emailExiste,
    dniExist,
    existeUsuarioPorId
}

