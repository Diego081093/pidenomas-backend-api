const { initializeApp } = require('firebase/app');

const firebaseConnect = async() => {
  try {
    const firebaseConfig = {
      apiKey: "AIzaSyAzALwVYpQBet384WpjbKaJoTFnHf7zPg0",
      authDomain: "pidenomaspe-4cbce.firebaseapp.com",
      databaseURL: "https://pidenomaspe-4cbce.firebaseio.com",
      projectId: "pidenomaspe-4cbce",
      storageBucket: "pidenomaspe-4cbce.appspot.com",
      messagingSenderId: "1015182605429",
      appId: "1:1015182605429:web:cc521972c6db1ea0c90c3c",
      measurementId: "G-099YJM2LQN"
    };
    
    initializeApp(firebaseConfig);

    console.log('Firebase listo');
  } catch (error) {
    console.log(error);
    throw new Error('Error a la hora de iniciar firebase');
  }
}

module.exports = {firebaseConnect}
